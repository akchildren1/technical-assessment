<?php
namespace App\Actions;

use Illuminate\Http\Request;
use SimpleXMLElement;
use tidy;

class XMLCleaner {

    /**
     * @vars
     */
    protected $request;
    protected $xml;

    /**
     * XMLCleaner constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return SimpleXMLElement | bool
     */
    public function execute()
    {
        $this->setXML();
        $this->cleanXML();
        $this->formatXML();

        libxml_use_internal_errors(true);
        return simplexml_load_string($this->xml);
    }

    private function setXML()
    {
        $this->xml = $this->request->get('xml');
    }

    /**
     * Tidy XML format
     */
    private function formatXML()
    {
        //Configuration for XML file
        $config = array(
            'input-xml'  => true,
            'output-xml' => true,
            'wrap'       => false);
        // Tidy
        $tidy = new tidy;
        $tidy->parseString($this->xml, $config);
        $tidy->cleanRepair();
        $this->xml = $tidy;
    }

    /**
     * Clean invalid XML
     */
    private function cleanXML()
    {
        $needle = '<?xml version="1.0" encoding="UTF-8"?>';
        $this->xml = str_replace(array($needle, '<Parent version="1.0">'), array('', '<Parent>'), $this->xml);

        foreach (['Request', 'Bacon'] as $tag) {
            $this->addSelfClosingTags($tag);
        }

        $this->addXMLWrapper($needle);
    }

    /**
     * Add closing tags to unclosed xml data
     * @param $name
     */
    private function addSelfClosingTags($name)
    {
        $pattern = "#(<\s*{$name}[^>]*)\s*>#";
        $replacement = "$1></$name>";
        $this->xml = preg_replace($pattern, $replacement, $this->xml);
    }

    /**
     * Wrap XML
     * @param $version
     */
    private function addXMLWrapper($version)
    {
        $this->xml= "$version<xmlset>$this->xml</xmlset>";
    }
}