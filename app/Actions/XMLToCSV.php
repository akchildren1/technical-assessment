<?php

namespace App\Actions;

class XMLToCSV
{

    /**
     * @var
     */
    protected $file;
    protected $rows;
    protected $header;

    /**
     * @param $simpleXML
     * @param $name
     * @return mixed
     */
    public function execute($simpleXML, $name)
    {
        $xml = $this->XMLToArray($simpleXML);
        $this->setRows($xml['Parent']);
        $this->createCSV($name);

        return $this->file;
    }

    /**
     * @param $xml
     * @return mixed
     */
    private function XMLtoArray($xml)
    {
        return json_decode(json_encode((array)$xml[0]), TRUE);
    }

    /**
     * @param $data
     */
    private function setRows($data)
    {
        $index = 0;
        foreach ($data as $key => $parent) {
            foreach ($parent as $set) {
                $this->setRow($index, $set);
            }
            $index++;
        }
    }

    /**
     * @param $index
     * @param $set
     */
    private function setRow($index, $set)
    {
        if (isset($set['@attributes'])) {
            $this->storeRowData($index, $set['@attributes']);
            unset($set['@attributes']);
        }
        if ($size = count($set)) {
            foreach ($set as $key => $value) {
                $this->storeRowData($index, [$key => $value]);
            }
        }
    }

    /**
     * Append data to csv row
     * @param $key
     * @param $value
     */
    private function storeRowData($key, $value)
    {
        $index = key($value);
        $this->rows[$key][$index] = $value[$index];
    }

    /**
     * @param $name
     */
    private function createCSV($name)
    {
        $this->file = fopen($name, 'wb');

        //Set CSV headers
        $this->setCSVHeader();
        fputcsv($this->file, $this->header);

        //Reorder columns for csv
        $this->rows = $this->reorderColumns();

        //Store/reorder data from array to be stored in CSV
        foreach($this->rows as $row){
            fputcsv($this->file, array_values($row));
        }
        fclose($this->file);
    }

    /**
     * Set order of the CSV columns
     */
    private function setCSVHeader()
    {
        $this->header = array('Timestamp', 'authIdentifier', 'indexType', 'IndexName', 'sandwich');
    }

    /**
     * Reorder CSV columns to match header
     * @return array
     */
    private function reorderColumns(): array
    {
        return array_map(function ($row) {
            return array_merge(array_flip($this->header), $row);
        }, $this->rows);
    }
}