<?php

namespace App\Http\Controllers;

use App\Actions\XMLCleaner;
use App\Actions\XMLToCSV;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class XMLController extends Controller
{
    /**
     * @param Request $request
     * @param XMLCleaner $cleaner
     * @param XMLToCSV $exporter
     * @return RedirectResponse|BinaryFileResponse
     */
    public function __invoke(Request $request, XMLCleaner $cleaner, XMLToCSV $exporter)
    {
        $this->validate($request, [
            'xml' => [
                'required',
                'string'
            ]
        ]);

        $xml = $cleaner->execute();

        if ($xml === false) {
            return $this->getFailedResponse();
        }

        $fileName = 'xml.csv';
        $exporter->execute($xml, $fileName);
        return response()->download($fileName);
    }

    /**
     * @return RedirectResponse
     */
    private function getFailedResponse(): RedirectResponse
    {
        $errors = [];
        foreach(libxml_get_errors() as $error) {
            $errors[] = $error->message;
        }
        return redirect()->back()->withErrors($errors);
    }
}