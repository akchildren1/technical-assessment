<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app" class="container">
    <h1>XML to CSV converter</h1>
    <form action="{{route('xml')}}" method="POST">
        {!! csrf_field() !!}
        <div class="form-group">
            
            <label for="xml">Insert XML</label>
            <textarea class="form-control form-control-lg"
                      id="xml"
                      name="xml"
                      placeholder="Insert text"
                      rows="15"
            ></textarea>

            <div class="btn-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>

        </div>
    </form>
    @if ($errors->any())
        <hr>
        <h2 class="error">There was an error with this process, please correct the following before resubmitting:</h2>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    @endif
</div>
@stack('scripts')
</body>
</html>
